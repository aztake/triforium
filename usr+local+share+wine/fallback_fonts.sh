echo "レジストリに書き込みを行っています。"
WINEPREFIX=${HOME}/.wine-pipelight WINE=/opt/wine-staging/bin/wine WINEARCH=win32 /opt/wine-staging/bin/regedit "Z:\\usr\\local\\share\\wine\\fallback_fonts.reg" > /dev/null 2>&1
result=$?
if [ ${result} -eq 0 ] ; then
	echo "処理は正常に完了しました。"
	echo "文字化けが再発した場合は、再度このファイルを実行してみてください。"
	echo （確認が完了したらEnterを押してください。）
	read ans
else
	echo "処理に失敗しました。"
	if [ ! -d "${HOME}/.wine-pipelight" ] ; then
		echo "Pipelightのインストール以来、まだブラウザーが起動されていません。"
	fi
	if [ ! -f "/usr/local/share/wine/fallback_fonts.reg" ] ; then
		echo "レジストリファイルが存在しません。"
	fi
fi
