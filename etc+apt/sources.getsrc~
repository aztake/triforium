
# See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
# newer versions of the distribution.
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty main restricted
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty main restricted

## Major bug fix updates produced after the final release of the
## distribution.
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates main restricted
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates main restricted

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu
## team. Also, please note that software in universe WILL NOT receive any
## review or updates from the Ubuntu security team.
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty universe
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates universe
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty universe
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates universe

## N.B. software from this repository is ENTIRELY UNSUPPORTED by the Ubuntu 
## team, and may not be under a free licence. Please satisfy yourself as to 
## your rights to use the software. Also, please note that software in 
## multiverse WILL NOT receive any review or updates from the Ubuntu
## security team.
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty multiverse
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates multiverse
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty multiverse
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-updates multiverse

## N.B. software from this repository may not have been tested as
## extensively as that contained in the main release, although it includes
## newer versions of some applications which may provide useful features.
## Also, please note that software in backports WILL NOT receive any review
## or updates from the Ubuntu security team.
deb http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-backports main restricted universe multiverse
deb-src http://ftp.jaist.ac.jp/pub/Linux/ubuntu/ trusty-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu trusty-security main restricted
deb http://security.ubuntu.com/ubuntu trusty-security universe
deb http://security.ubuntu.com/ubuntu trusty-security multiverse
deb-src http://security.ubuntu.com/ubuntu trusty-security main restricted
deb-src http://security.ubuntu.com/ubuntu trusty-security universe
deb-src http://security.ubuntu.com/ubuntu trusty-security multiverse

## Uncomment the following two lines to add software from Canonical's
## 'partner' repository.
## This software is not part of Ubuntu, but is offered by Canonical and the
## respective vendors as a service to Ubuntu users.
deb http://archive.canonical.com/ubuntu trusty partner
deb-src http://archive.canonical.com/ubuntu trusty partner

## Uncomment the following two lines to add software from Ubuntu's
## 'extras' repository.
## This software is not part of Ubuntu, but is offered by third-party
## developers who want to ship their latest software.
deb http://extras.ubuntu.com/ubuntu trusty main
deb-src http://extras.ubuntu.com/ubuntu trusty main




#=====================================================================================
#  14.04 Trusty Tahr + Debian APT
#=====================================================================================

## Japanese Team
deb http://archive.ubuntulinux.jp/ubuntu/ trusty main
deb http://archive.ubuntulinux.jp/ubuntu-ja-non-free/ trusty multiverse
deb-src http://archive.ubuntulinux.jp/ubuntu/ trusty main
deb-src http://archive.ubuntulinux.jp/ubuntu-ja-non-free/ trusty multiverse

## LibreOffice
deb http://ppa.launchpad.net/libreoffice/ppa/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/libreoffice/ppa/ubuntu/ trusty main

## JDownloader
deb http://ppa.launchpad.net/jd-team/jdownloader/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/jd-team/jdownloader/ubuntu/ trusty main

## Grub Customizer
deb http://ppa.launchpad.net/danielrichter2007/grub-customizer/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/danielrichter2007/grub-customizer/ubuntu/ trusty main

## Firefox Aurora
deb http://ppa.launchpad.net/ubuntu-mozilla-daily/firefox-aurora/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/ubuntu-mozilla-daily/firefox-aurora/ubuntu/ trusty main

## Ubuntuzilla
# deb http://downloads.sourceforge.net/project/ubuntuzilla/mozilla/apt all main

## Google Chrome
# deb http://dl.google.com/linux/chrome/deb/ stable main

## Opera
deb http://deb.opera.com/opera/ stable non-free
deb http://deb.opera.com/opera-beta/ stable non-free

## GetDeb
# deb http://ftp.heanet.ie/mirrors/www.getdeb.net/getdeb/ubuntu/ trusty-getdeb apps
# deb http://ftp.heanet.ie/mirrors/www.getdeb.net/getdeb/ubuntu/ trusty-getdeb games
# deb-src http://ftp.heanet.ie/mirrors/www.getdeb.net/getdeb/ubuntu/ trusty-getdeb apps
# deb-src http://ftp.heanet.ie/mirrors/www.getdeb.net/getdeb/ubuntu/ trusty-getdeb games

## libdvdcss
deb http://download.videolan.org/pub/debian/stable/ /
deb-src http://download.videolan.org/pub/debian/stable/ /

## Wine
deb http://ppa.launchpad.net/ubuntu-wine/ppa/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/ubuntu-wine/ppa/ubuntu/ trusty main

## Oracle JRE
deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main
deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main

## CDEmu
deb http://ppa.launchpad.net/cdemu/ppa/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/cdemu/ppa/ubuntu/ trusty main

## Intel Graphics drivers
# deb https://download.01.org/gfx/ubuntu/14.04/main trusty main

## Obkey
deb http://ppa.launchpad.net/alex-p/notesalexp-trusty/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/alex-p/notesalexp-trusty/ubuntu/ trusty main

## ClipGrab
deb http://ppa.launchpad.net/clipgrab-team/ppa/ubuntu trusty main
deb-src http://ppa.launchpad.net/clipgrab-team/ppa/ubuntu trusty main

## Skippy-XD
deb http://ppa.launchpad.net/landronimirc/skippy-xd-daily/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/landronimirc/skippy-xd-daily/ubuntu/ trusty main

## Nevernote
deb http://ppa.launchpad.net/vincent-c/nevernote/ubuntu/ trusty main
deb-src http://ppa.launchpad.net/vincent-c/nevernote/ubuntu/ trusty main

# Boot Repair
deb http://ppa.launchpad.net/yannubuntu/boot-repair/ubuntu trusty main
deb-src http://ppa.launchpad.net/yannubuntu/boot-repair/ubuntu trusty main

# MakeMKV
deb http://ppa.launchpad.net/heyarje/makemkv-beta/ubuntu trusty main
deb-src http://ppa.launchpad.net/heyarje/makemkv-beta/ubuntu trusty main

# FreshPlayer
deb http://ppa.launchpad.net/nilarimogard/webupd8/ubuntu trusty main
deb-src http://ppa.launchpad.net/nilarimogard/webupd8/ubuntu trusty main

# Pepper Flash
deb http://ppa.launchpad.net/skunk/pepper-flash/ubuntu trusty main
deb-src http://ppa.launchpad.net/skunk/pepper-flash/ubuntu trusty main

# Pipelight
deb http://ppa.launchpad.net/pipelight/stable/ubuntu trusty main
deb-src http://ppa.launchpad.net/pipelight/stable/ubuntu trusty main

# SMTube
deb http://ppa.launchpad.net/rvm/smplayer/ubuntu trusty main
deb-src http://ppa.launchpad.net/rvm/smplayer/ubuntu trusty main




#=====================================================================================
#  13.10 Saucy Salamander
#=====================================================================================

## fake-pae
# deb http://ppa.launchpad.net/prof7bit/fake-pae/ubuntu saucy main
# deb-src http://ppa.launchpad.net/prof7bit/fake-pae/ubuntu saucy main




#=====================================================================================
#  13.04 Raring Ringtail
#=====================================================================================

## Faenza Icon
deb http://ppa.launchpad.net/tiheum/equinox/ubuntu/ raring main
deb-src http://ppa.launchpad.net/tiheum/equinox/ubuntu/ raring main

## PPA Software Center
deb http://ppa.launchpad.net/marcoscarpetta/ppasc/ubuntu/ raring main
deb-src http://ppa.launchpad.net/marcoscarpetta/ppasc/ubuntu/ raring main




#=====================================================================================
#  12.10 Quantal Quetzal
#=====================================================================================

## Remastersys
# deb http://geekconnection.org/remastersys/ubuntu quantal main

## xrdp ja
deb http://ppa.launchpad.net/ikuya-fruitsbasket/xrdp/ubuntu/ quantal main
deb-src http://ppa.launchpad.net/ikuya-fruitsbasket/xrdp/ubuntu/ quantal main




#=====================================================================================
#  12.04 Precise Pangolin
#=====================================================================================

## SCIM-GTK3
# deb http://ppa.launchpad.net/ikoinoba/ppa/ubuntu/ precise main
# deb-src http://ppa.launchpad.net/ikoinoba/ppa/ubuntu/ precise main

