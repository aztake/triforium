#!/bin/bash

if [ "$1" == "sync" ] ; then
	_ntp()
	{
		echo 0
		ntpdate ntp.nict.jp
		hwclock -w
	}
	
	wmctrl -c "linuxBean 日付と時刻" 
	
	_ntp | \
	yad --title="調整中" --on-top --center \
	--window-icon=preferences-system-time --image=preferences-system-time \
	--text "ntp.nict.jpに接続しています..." --progress --pulsate --auto-close
	
	yad --title="完了" --on-top --center \
	--window-icon=preferences-system-time --image=preferences-system-time \
	--text "調整が完了しました。"
	
	exit 0

elif [ "$1" == "adjust" ] ; then
	sed -i -e "s|^UTC=.*$|$4|" "/etc/default/rcS"
	
	if [ $# -gt 4 ] ; then
		echo "${5}" > "/etc/timezone"
		ln -s -f "/usr/share/zoneinfo/${5}" "/etc/localtime"
		dpkg-reconfigure -f noninteractive tzdata
	fi
	
	date -s "${2} ${3}"
	hwclock -w
	
	yad --title="完了" --on-top --center \
	--window-icon=preferences-system-time --image=preferences-system-time \
	--text "調整が完了しました。"
	
	exit 0
fi

br="
"

if [ "$(cat '/etc/default/rcS' | grep '^UTC=')" == "UTC=yes" ] ; then
	utc="TRUE"
else
	utc="FALSE"
fi

tz=$(cat "/etc/timezone")
list=${tz}${br}$(find /usr/share/zoneinfo -type f | sed -e "s|^/usr/share/zoneinfo/||igm" | \
	grep -v "^[^/]*$" | grep -v "^Etc" | grep -v "^posix" | grep -v "^right" | grep -v "SystemV" | sort)


result=$(\
	echo "${list}" | yad --plug=8901016 --tabnum=1 \
	--calendar --date-format=%Y/%m/%d &
	
	echo "${list}" | yad --plug=8901016 --tabnum=2 \
	--form \
	--field="時:NUM" "$(date '+%H')!0..23!1" \
	--field="分:NUM" "$(date '+%M')!0..59!1" \
	--field="秒:NUM" "$(date '+%S')!0..59!1" \
	--field=":LBL" "" \
	--field="UTC（世界協定時）を使用する:CHK" "${utc}" \
	--field="<span foreground='#990000'>多くのWindows機ではUTCは必要ありません。\n時刻が9時間ずれた際にのみ変更してください。</span>:LBL" "" &
	
	echo "${list}" | yad --plug=8901016 --tabnum=3 \
	--text "現在のタイムゾーン: $tz" \
	--list --column="タイムゾーン" --no-headers &
	
	echo "${list}" | yad --plug=8901016 --tabnum=4 \
	--form \
	--field="\n<span font='sans 7'> </span>\nnict.jp（独立行政法人 情報通信研究機構）に接続し、\n自動で時刻合わせを行います。\n<span font='sans 4'> </span>\nタイムゾーンの変更が必要な場合は、\nあらかじめリストから選択しOKを押すことで\n内容を確定させておいてください。\n<span font='sans 5'> </span>:LBL" "" \
	--field="【　　　　　　　自動調整開始　　　　　　　】:BTN" "gksudo $0 sync & exit 0" &
	
	yad --key=8901016 \
	--width="320" --on-top --center \
	--title="linuxBean 日付と時刻" --window-icon=preferences-system-time \
	--text="  日付と時刻の調整を行います。\n  OKで内容を確定、キャンセルで変更を破棄します。" \
	--notebook --tab="日付" --tab="時刻" --tab="タイムゾーン" --tab="自動調整"\
)

# \n<span font='sans 4'> </span>\nまた、自動調整が完了したらそれ以上調整せず、\nキャンセルを押してウィンドウを閉じてください。

if [ $? -gt 0 ] || [ "$result" == "" ] ; then
	exit 1
fi

yyyy=$(echo "${result}" | grep "^[0-9][0-9][0-9][0-9]" | cut -d/ -f1)
mm=$(echo "${result}" | grep "^[0-9][0-9][0-9][0-9]" | cut -d/ -f2)
dd=$(echo "${result}" | grep "^[0-9][0-9][0-9][0-9]" | cut -d/ -f3)

hh=$(echo "${result}" | grep -v "^[0-9][0-9][0-9][0-9]" | grep "^[0-9]" | cut -d\| -f1 | cut -d. -f1)
mi=$(echo "${result}" | grep -v "^[0-9][0-9][0-9][0-9]" | grep "^[0-9]" | cut -d\| -f2 | cut -d. -f1)
ss=$(echo "${result}" | grep -v "^[0-9][0-9][0-9][0-9]" | grep "^[0-9]" | cut -d\| -f3 | cut -d. -f1)

utc=$(echo "${result}" | grep -v "^[0-9][0-9][0-9][0-9]" | grep "^[0-9]" | cut -d\| -f5)
if [ "${utc}" == "TRUE" ] ; then
	utc="UTC=yes"
else
	utc="UTC=no"
fi

_tz=""
_tz=$(echo "${result}" | grep "^[a-zA-Z]" | cut -d\| -f1)

gksudo $0 adjust ${yyyy}/${mm}/${dd} ${hh}:${mi}:${ss} ${utc} ${_tz}
