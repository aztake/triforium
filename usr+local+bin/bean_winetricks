#!/bin/bash
. bean_func.fnc

# Winetricksの存在確認
if [ $(Bean_CmdExist winetricks) -gt 0 ] ; then
	yad --title="エラー" --text "パッケージ winetricks をインストールしてください。" \
	--window-icon=wine-winetricks --image=application-exit --on-top --center
	exit 1
fi

# パスの設定
cache=${HOME}/.cache/bean_winetricks
mkdir -p "${cache}"

# 改行の置換
br="
"
	
# リストを表示可能な形式に変換
_cache_list()
{
	local list=$(winetricks $1 list)
	
	echo "${list}" > "${cache}/$1_base.txt"
	
	echo "${list}" | sed \
	-e 's/^\([^ ]*\) */FALSE\n\1\n/igm' \
	-e 's/ \[[(downloadable),(cached)]*\]$//' \
	> "${cache}/$1.txt"
}

# キャッシュが存在する場合はリストの作成をスキップする
_check_list()
{
	if [ ! -e "${cache}/$1.txt" ] ; then
		_cache_list $1
	fi
}

# インストール済アプリのリストを作成
_cache_installed()
{
	local instlist
	if [ ! -e "${cache}/installed.txt" ] ; then
		instlist=$(winetricks list-installed | sed -e 's/^/-e /' | awk -F"\n" -v ORS=" " '{print}')
		
		cd "${cache}"
		ls | grep _base.txt$ | xargs grep ${instlist} | sed \
		-e 's/^.*://' -e 's/^\([^ ]*\) *\(.*\)$/FALSE\n\1\n\2\n\+\-\+installed\+\-\+/igm' \
		-e 's/ \[[(downloadable),(cached)]*\]//' \
		> "${cache}/installed.txt"
	fi
}

# リストの作成処理
_dlls_list()
{
	echo 0
	echo 5
	
	if [ ! "$(cat ${cache}/version.txt )" == "$(winetricks -V)" ] ; then
		echo 10
		_cache_list dlls       ; echo 20
		_cache_list settings   ; echo 35
		_cache_list apps       ; echo 50
		_cache_list benchmarks ; echo 60
		_cache_list games      ; echo 75
		_cache_list fonts      ; echo 85
		rm -f "${cache}/installed.txt" 
	else
		echo 60
		_check_list dlls       ; echo 65
		_check_list settings   ; echo 70
		_check_list apps       ; echo 75
		_check_list benchmarks ; echo 80
		_check_list games      ; echo 85
		_check_list fonts      ; echo 90
	fi
	
	_cache_installed
	
	winetricks -V > "${cache}/version.txt"
	echo 100
}

# キャッシュの削除
if [ "$1" == "--clean" ] ; then
	rm -rf "${cache}" && yad \
	--on-top --center --title="キャッシュの初期化" \
	--window-icon=wine-winetricks --image=wine-winetricks \
	--text "全リストのキャッシュを削除しました。\n次回起動時に再構築が行われます。"
	exit 0

elif [ "$1" == "--refresh" ] ; then
	rm -f "${cache}/installed.txt" && yad \
	--on-top --center --title="キャッシュの初期化" \
	--window-icon=wine-winetricks --image=wine-winetricks \
	--text "インストール済リストのキャッシュを削除しました。\n次回起動時に再構築が行われます。"
	exit 0
	
# 実際のインストール処理
elif [ $# -gt 0 ] ; then
	err=0
	for i in "$@" ; do
		echo $i をインストールします。
		echo
		
		winetricks $i
		if [ $? -gt 0 ] ; then
			echo $i のインストールに失敗しました。
			err=1
		else
			echo $i のインストールが完了しました。
		fi
		echo
	done
	
	echo キャッシュを再構築しています...
	rm -f "${cache}/installed.txt" 
	_check_list dlls
	_check_list settings
	_check_list apps
	_check_list benchmarks
	_check_list games
	_check_list fonts
	_cache_installed
	
	echo キーを押すとウィンドウを閉じます。
	read ans
	exit $err
fi

# 作成中のプログレス表示
if [ ! -e "${cache}/dlls.txt" ] ; then
	_dlls_list | yad --title="キャッシュの構築中" --on-top --center \
	--window-icon=wine-winetricks --image=wine-winetricks \
	--progress --auto-close --text "リストを表示するために必要な情報を収集しています。\nかなり時間が掛かりますので気長にお待ちください。\n次回以降はキャッシュされるので多少起動が早くなります。\n（※ボタンを押すとウィンドウを隠します）"
else
	_dlls_list | yad --title="変換中" --on-top --center \
	--window-icon=wine-winetricks --image=wine-winetricks \
	--progress --auto-close --text "リストを表示するために必要な情報を収集しています。\nかなり時間が掛かりますので気長にお待ちください。\n（※ボタンを押すとウィンドウを隠します）"
fi

# リストの表示
checked=$(
	cat "${cache}/dlls.txt" | \
	yad --plug=1341398 --tabnum=1 \
	--text "インストールしたいDLLにチェックを入れてください。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/settings.txt" | \
	yad --plug=1341398 --tabnum=2 \
	--text "変更したい設定にチェックを入れてください。重複した場合は下にあるものが優先されます。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/apps.txt" | \
	yad --plug=1341398 --tabnum=3 \
	--text "インストールしたいアプリにチェックを入れてください。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/games.txt" | \
	yad --plug=1341398 --tabnum=4 \
	--text "インストールしたいゲームにチェックを入れてください。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/fonts.txt" | \
	yad --plug=1341398 --tabnum=5 \
	--text "インストールしたいフォントにチェックを入れてください。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/benchmarks.txt" | \
	yad --plug=1341398 --tabnum=6 \
	--text "インストールしたいベンチマークソフトにチェックを入れてください。" \
	--list --checklist --column="DL" --column="オプション" --column="説明" &
	
	cat "${cache}/installed.txt" | \
	yad --plug=1341398 --tabnum=7 --hide-column="4" \
	--text "インストール済アプリのリストです。変更しても何も起こりません。\n\n（※コマンドラインからWinetricksを使うと記録に不整合が起きるため、\n「ツール＞インストール済リストの初期化」を使用してキャッシュの再構築を行ってください。）" \
	--list --checklist --column="DL" --column="オプション" --column="説明" --column="種別" &
	
	yad --plug=1341398 --tabnum=8 \
	--image=wine-winetricks \
	--text "\n<span weight='bold' size='x-large' underline='single'>エラーが出た時は？\n\n</span>Winetricks自体が古くなっていると、一部の項目でダウンロード先が移転しまっている事があります。\n\nそういう場合は、PPAを利用してWinetricks自体を最新に保つか、手動でダウンロードしたインストーラーを\n${HOME}/.cache/winetricksに配置することで動作させられるかもしれません。\n\n──────────\n\n<span weight='bold' size='x-large' underline='single'>キャッシュの不整合\n\n</span>コマンドラインからWinetricksを使用すると、フロントエンドのキャッシュが更新されず不整合を起こしてしまいます。\n\nインストール済アプリの一覧を使用しないのであれば無視しても問題無いのですが、一応<span weight='bold' foreground='#FF0000' background='#FFFF99'>↓このボタン</span>を押すことで次回起動時にキャッシュの再構築を行われるので、気になる場合は押してみてください。\n" \
	--form --field "インストール済リストの初期化（${HOME}/.cache/bean__winetricks/installed.txtを削除します）:BTN" "$0 --refresh" \
	--form --field "全リストの初期化（${HOME}/.cache/bean__winetricksをフォルダごと削除します）:BTN" "$0 --clean" &

	yad --key=1341398 \
	--width="640" --height="480" --on-top --center \
	--title="linuxBean Winetricks フロントエンド" --window-icon=wine-winetricks \
	--notebook --tab="DLL" --tab="設定" --tab="アプリ" --tab="ゲーム" \
	--tab="フォント" --tab="ベンチマーク" --tab="インストール済" --tab="ツール"
)

# キャンセルした場合は終了
if [ $? -gt 0 ] ; then
	exit 1
fi

# 不要な部分を削除
checked=$(echo "${checked}" | grep -v "\+\-\+.*\+\-\+" | grep -v "^|*$")

# チェックを入れていない場合は終了
if [ "${checked}" == "" ] ; then
	exit 1
fi

# 確認
desclist=$(echo "${checked}" | awk -F"|" '{print $3}')
yad --title="確認" --on-top --center --width=550 \
--window-icon=wine-winetricks --image=wine-winetricks \
--text "以下の変更を適用しますか？${br}${br}${desclist}"

# キャンセルした場合は終了
if [ $? -gt 0 ] ; then
	exit 1
fi

# 端末窓に渡すための整形
checked=$(echo "${checked}" | awk -F"|" '{print $2}' | awk -F"\n" -v ORS=" " '{print}')

# 残りの処理は別窓に渡す
x-terminal-emulator -e "$0 ${checked}"
